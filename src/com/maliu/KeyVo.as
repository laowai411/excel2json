package com.maliu
{
	
	/**
	 * @author maliu 2013-11-19
	 */
	public class KeyVo
	{
		public static const TYPE_BASIC:int = 0;
		public static const TYPE_ARRAY:int = 1;
		public static const TYPE_OBJECT:int = 2;
		
		public var keyName:String;
		
		public var parent:KeyVo;
		public var childrenList:Array; //子节点集合
		public var arrayChildrenSet:Array; //Array类型子节点的集合
		/**类型：0：基本值类型 1：Array类型 2：Object类型*/
		public var type:int;
		private var _rowLength:int; //运行时所占行数的动态长度（list才用到，每个根object都需要重置）
		
		private var _row:int;
		private var _col:int;
		
		private var _appearCount:int = 0;
		private var _startIndex:int;
		private var _endIndex:int;
		
		/**
		 * 数据模型
		 */
		public function KeyVo($keyName:String)
		{
			this.keyName = $keyName;
		}

		public function get rowLength():int
		{
			return _rowLength;
		}

		public function set rowLength(value:int):void
		{
			_rowLength = value;
		}

		public function appear($row:int, $col:int):void
		{
			if(_appearCount < 1) //第一次出现
			{
				_row = $row;
				_col = $col;
				_startIndex = _endIndex = $col;
			}
			else
			{
				_endIndex = $col;
			}
			_appearCount++;
		}
		public function addChildVo($childKeyVo:KeyVo):void
		{
			if(!childrenList)
			{
				childrenList = [];
			}
			if(childrenList.indexOf($childKeyVo) == -1)
			{
				childrenList.push($childKeyVo);
			}
			if($childKeyVo.type == TYPE_ARRAY || $childKeyVo.type == TYPE_OBJECT)
			{
				if(!arrayChildrenSet)
				{
					arrayChildrenSet = [];
				}
				if(arrayChildrenSet.indexOf($childKeyVo) == -1)
				{
					arrayChildrenSet.push($childKeyVo);
				}
			}
		}
		public function get startIndex():int
		{
			return _startIndex;
		}
		public function get endIndex():int
		{
			return _endIndex;
		}
		public function get lenght():int
		{
			return _appearCount;
		}
		public function get row():int
		{
			return _row;
		}
		public function get col():int
		{
			return _col;
		}
		public function get maxListChildRowLength():int
		{
			arrayChildrenSet.sortOn("rowLength",Array.NUMERIC | Array.DESCENDING); 
			return arrayChildrenSet[0].rowLength;
		}
	}
	
}
