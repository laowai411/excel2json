package com.maliu
{
	
	/**
	 * @author maliu 2013-11-19
	 */
	public class JsonUtil
	{
		public function JsonUtil()
		{
		}
//===================================== API =====================================
		/**
		 * 解析字符串为具体的值
		 * @param $str
		 * @return 
		 */		
		public static function stringToValue($str:String):*
		{
			if($str == null)
				return null;
			if($str.indexOf("|") != -1)
			{
				return $str.split("|"); //返回数组
			}
			else if(isNaN(Number($str)))
			{
				return $str; //返回字符串
			}
			else
			{
				return Number($str); //返回Number
			}
			return null;
		}
		/**
		 * object转换为json
		 * @param $obj
		 * @param $useFormatter 输出时候是否格式化json字符串（格式化后便于阅读，但体积会增大）
		 * @return 
		 */		
		public static function objectToJson($obj:Object, $useFormatter:Boolean = true):String
		{
			var t_formatter:String;
			if($useFormatter)
				t_formatter = "\t";
			return JSON.stringify($obj,null,t_formatter);
		}
		/**
		 * json转换为object
		 * @param $json
		 * @return 
		 */		
		public static function jsonToObject($json:String):Object
		{
			return JSON.parse($json);
		}
//=============================== private method ================================
//=================================== Handler ===================================
	}
}