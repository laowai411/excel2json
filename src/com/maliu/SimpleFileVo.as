package com.maliu
{
	
	/**
	 * @author maliu 2013-11-19
	 */
	public class SimpleFileVo
	{
		
		/**
		 * 文件路径
		 * */
		public var url:String;
		
		/**
		 * 文件名
		 * */
		public var fileName:String;
		
		public function SimpleFileVo()
		{
		}
//===================================== API =====================================
//=============================== private method ================================
//=================================== Handler ===================================
	}
}