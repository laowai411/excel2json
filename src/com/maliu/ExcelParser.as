package com.maliu
{
	import com.lipi.excel.Excel;
	import com.maliu.debug.LogManager;
	
	import flash.geom.Point;
	import flash.utils.ByteArray;
	
	/**
	 * Excel解析器
	 * @author maliu 2013-11-19
	 */
	public class ExcelParser
	{
		private var _data:ByteArray;
		private var _sourceSheet:Array; //转换后的原始二维数组
		
		private var _outputFileName:String; //输出的文件名
		private var _outputFlieUrl:String; //输出的地址
		private var _outputObj:Object = {}; //输出为json的object
		private var _keyMap:Object = {}; //keyName-keyVo
		private var _keyLevel:int = 0;
		private var _colKeyNameArray:Array = [];
		private var _nestCount:int; //进入嵌套类型的计量数，进入嵌套数据类型（Object/Array）时候+1，退出嵌套时候-1
		
		//当前正在遍历的excel排坐标和列坐标（列坐标用“A”、“B”、“C”...表示）
		private var _curRowIndex:String;
		private var _curColIndex:String;
		
		private static const OUTPUT_NAME_INDEX:Point = new Point(0,2);
		private static const OUTPUT_URL_INDEX:Point = new Point(1,2);
		
		public function ExcelParser()
		{
		}
//===================================== API =====================================
		public function get outputObj():Object
		{
			return _outputObj;
		}
		public function get outputFlieUrl():String
		{
			return _outputFlieUrl;
		}
		public function get outputFileName():String
		{
			return _outputFileName;
		}
		public function setData($data:ByteArray):void
		{
			_data = $data;
			start();
		}
		/**
		 * 前端配置表名
		 * */
		public function get clientFileName():String
		{
			return "";
		}
		
		/**
		 * 前端配置表路径
		 * */
		public function get clientFilePath():String
		{
			return "";
		}
		
		/**
		 * 后端配置表名
		 * */
		public function get serverFileName():String
		{
			return "";
		}
		
		/**
		 * 后端配置表路径
		 * */
		public function get serverFilePath():String
		{
			return "";
		}
//=============================== private method ================================
		private function start():void
		{
			var t_excel:Excel = new Excel(_data);
			_sourceSheet = t_excel.getSheetArray();
			
			parseExcelArray(_sourceSheet);
			
			_outputFileName = _sourceSheet[OUTPUT_NAME_INDEX.x][OUTPUT_NAME_INDEX.y];
			_outputFlieUrl = _sourceSheet[OUTPUT_URL_INDEX.x][OUTPUT_URL_INDEX.y];
		}
		/**
		 * 解析excel转换过来的原始二维数组
		 * @param $arr
		 */		
		private function parseExcelArray($arr:Array):void
		{
			var i:int = 0;
			var j:int = 0;
			var t_n:int = $arr.length;
			var t_mainKeyName:String;
			for(i=0; i<t_n; i++)
			{
				if($arr[i][0] == "comment")
				{
					continue;
				}
				else if($arr[i][0] == "key")
				{
					parseKeyArray($arr[i], i);
				}
				else
				{
					t_mainKeyName = $arr[i][1];
					if(t_mainKeyName == null)
					{
						continue;
					}
					else
					{
						resetRowLength();
						_outputObj[t_mainKeyName] = loopValue(i, 1, $arr[i].length-1);
					}
				}
			}
		}
		/**解析key*/
		private function parseKeyArray($keyArray:Array, $row:int):void
		{
			var i:int = 1;
			var t_n:int = $keyArray.length;
			var t_keyName:String;
			var t_type:int;
			for(; i<t_n; i++)
			{
				t_type = KeyVo.TYPE_BASIC;
				t_keyName = $keyArray[i];
				if(t_keyName != null)
				{
					//Array和Object的特殊键作特殊处理
					if(t_keyName.indexOf("[]") != -1)
					{
						t_keyName = t_keyName.replace("[]","");
						t_type = KeyVo.TYPE_ARRAY;
					}
					else if(t_keyName.indexOf("{}") != -1)
					{
						t_keyName = t_keyName.replace("{}","");
						t_type = KeyVo.TYPE_OBJECT;
					}
					
					if(!_colKeyNameArray[i])
					{
						_colKeyNameArray[i] = [];
					}
					_colKeyNameArray[i][_keyLevel] = t_keyName;
					
					var t_keyVo:KeyVo = getKeyVo(t_keyName);
					t_keyVo.type = t_type;
					t_keyVo.appear($row, i);
					
					if(_keyLevel > 0)
					{
						var t_parentKeyName:String = convertKeyStrToKeyName(_sourceSheet[$row-1][i]);
						var t_parentKeyVo:KeyVo = getKeyVo(t_parentKeyName);
						t_keyVo.parent = t_parentKeyVo;
						t_parentKeyVo.addChildVo(t_keyVo);
					}
				}
			}
			_keyLevel++;
		}
		private function convertKeyStrToKeyName($keyStr:String):String
		{
			var t_keyName:String;
			if($keyStr.indexOf("[]") != -1)
			{
				t_keyName = $keyStr.replace("[]","");
			}
			else if($keyStr.indexOf("{}") != -1)
			{
				t_keyName = $keyStr.replace("{}","");
			}
			else
			{
				t_keyName = $keyStr;
			}
			return t_keyName;
		}
		/**
		 * 通过键名返回数据模型
		 * @param $keyName
		 */		
		private function getKeyVo($keyName:String):KeyVo
		{
			if(!_keyMap[$keyName])
			{
				_keyMap[$keyName] = new KeyVo($keyName);
			}
			return _keyMap[$keyName];
		}
		/**
		 * 横向解析value
		 * @param $row 所在排数
		 * @param $startIndex 开始列index
		 * @param $endIndex 结束列index
		 * @return 一个object的value值
		 */		
		private function loopValue($row:int, $startIndex:int, $endIndex:int):Object
		{
			var t_obj:Object = {};
			var t_str:String;
			var t_keyNameList:Array;
			var t_keyName:String;
			var t_keyVo:KeyVo;
			for(var i:int = $startIndex; i<=$endIndex; i++)
			{
				_curRowIndex = String($row+1);
				_curColIndex = String.fromCharCode(("A").charCodeAt()+i);
				
				t_str = _sourceSheet[$row][i];
				t_keyNameList = _colKeyNameArray[i];
				t_keyName = t_keyNameList[_nestCount];
				t_keyVo = getKeyVo(t_keyName);
				LogManager.record(_nestCount, t_keyNameList.length-1, "|", _curRowIndex, _curColIndex, "|", t_str);
				t_keyVo.rowLength = 1;
				switch(t_keyVo.type)
				{
					case KeyVo.TYPE_ARRAY:
						t_obj[t_keyName] = loopList(t_keyVo, $row, t_str);
						break;
					case KeyVo.TYPE_OBJECT:
						t_obj[t_keyName] = loopObject(t_keyVo, $row);
						break;
					case KeyVo.TYPE_BASIC:
						t_obj[t_keyName] = JsonUtil.stringToValue(t_str);
						break;
				}
				i = t_keyVo.endIndex; //列index跳到endIndex结尾继续遍历
			}
			return t_obj;
		}
		/**
		 * 竖向进入数组解析数组
		 * @param $keyVo 
		 * @param $row
		 * @param $str
		 * @return 一个数组
		 */		
		private function loopList($keyVo:KeyVo, $row:int, $str:String):Array
		{
			if(!$keyVo.childrenList || $keyVo.childrenList.length < 1)
			{
				if($str == null)
				{
					return null;
				}
				else
				{
					return $str.split("|");
				}
			}
			if($str == "[]")
			{
				return [];
			}
			_nestCount++; //进入竖向循环，计量数+1
			var t_list:Array = [];
			var t_rowIndex:int = $row + $keyVo.rowLength; //遇到“[”下移一行开始遍历
			while(_sourceSheet[t_rowIndex][$keyVo.startIndex] != "]") //遇到“]”停止遍历
			{
				t_list.push(loopValue(t_rowIndex, $keyVo.startIndex, $keyVo.endIndex));
				if($keyVo.arrayChildrenSet)
				{
					$keyVo.rowLength += $keyVo.maxListChildRowLength;
				}
				else
				{
					$keyVo.rowLength++;
				}
				t_rowIndex = $row + $keyVo.rowLength;
			}
			$keyVo.rowLength++; //算上“[”这一行
			_nestCount--; //离开竖向循环，计量数-1
			return t_list;
		}
		/**
		 * 解析为Object的复杂类型
		 */
		private function loopObject($keyVo:KeyVo, $row:int):Object
		{
			_nestCount++; //进入竖向循环，计量数+1
			var t_obj:Object = {};
			var t_nullFlag:Boolean = true;
			t_obj = loopValue($row,$keyVo.startIndex,$keyVo.endIndex);
			if($keyVo.arrayChildrenSet)
			{
				$keyVo.rowLength = $keyVo.maxListChildRowLength;
			}
			for(var k:String in t_obj)
			{
				if(t_obj[k] === null)
				{}
				else
				{
					t_nullFlag = false;
					break;
				}
			}
			_nestCount--; //离开竖向循环，计量数-1
			if(t_nullFlag)
			{
				//全部属性都为null时候，整个object设置为null
				t_obj = null;
			}
			return t_obj;
		}
		private function resetRowLength():void
		{
			for(var k:String in _keyMap)
			{
				(_keyMap[k] as KeyVo).rowLength = 1;
			}
		}
//=================================== Handler ===================================
	}
}